var theoplayerSession = {
  registerPlayer: function registerPlayer(player) {
    var sessionData = null;
    var sessionSource = null;
    var streamIsUnavailable = false;
    var sessionStreamInterval;
    var countdownTimerInterval;
    var isWaiting = false;
    var hasInterval = false;
    var statechangeCallback = null;
    var datachangeCallback = null;
    var queryParameter = null;

    function handleSessionOffline() {
      statechangeCallback({
        state: "unavailable",
        info: "The configured stream is currently unavailable."
      });
      streamIsUnavailable = true;
      createStreamStatusDiv();

      if (!hasInterval) {
        hasInterval = true;
        sessionStreamInterval = setInterval(streamInterval, getRetryTimeout());
      }
    }

    function getRetryTimeout() {
      return sessionSource && sessionSource.retry || 3000;
    }

    function getOfflineText() {
      return sessionSource && sessionSource.offlineText || "The stream is currently not available.";
    }

    function setupCountdown(countDownDate) {
      var countDownDate = countDownDate.getTime();
      countdownTimerInterval = setInterval(function () {
        var now = new Date().getTime();
        var distance = countDownDate - now;
        var days = Math.floor(distance / (1000 * 60 * 60 * 24));
        var hours = Math.floor(distance % (1000 * 60 * 60 * 24) / (1000 * 60 * 60));
        var minutes = Math.floor(distance % (1000 * 60 * 60) / (1000 * 60));
        var seconds = Math.floor(distance % (1000 * 60) / 1000);
        getContainer().querySelector('.countdown').innerHTML = days + "d " + hours + "h " + minutes + "m " + seconds + "s ";

        if (distance < 0) {
          clearInterval(countdownTimerInterval);
          getContainer().querySelector('.countdown').innerHTML = "Starting...";
        }
      }, 1000);
    }

    function createStreamStatusDiv() {
      var streamStatusDiv = getContainer().querySelector('.stream-status');

      if (!streamStatusDiv) {
        streamStatusDiv = document.createElement('div');
        streamStatusDiv.className = 'stream-status';
        var poster = getPoster();

        if (poster) {
          streamStatusDiv.style.background = 'url("' + poster + '")';
          streamStatusDiv.style.backgroundSize = 'cover';
        }

        if (sessionData) {
          var countdown = sessionData['countdown_timestamp'];

          if (countdown) {
            countdown = new Date(parseInt(countdown) * 1000);

            if (countdown.getTime() > Date.now()) {
              // in the future
              var countdownDiv = document.createElement('div');
              countdownDiv.className = "stream-status-overlay";
              var date = countdown.toDateString();
              var hhmm = countdown.toISOString().substr(11, 5);
              var title = sessionData['title'] + "<br />" || false;
              countdownDiv.innerHTML = title + "Stream will start on:<br />" + date + " @ " + hhmm;
              streamStatusDiv.appendChild(countdownDiv);
              var countdownTimer = document.createElement('div');
              countdownTimer.className = 'countdown';
              countdownTimer.innerHTML = "&nbsp;";
              countdownDiv.appendChild(countdownTimer);
              setupCountdown(countdown);
            }
          }
        } else {
          streamStatusDiv.textContent = getOfflineText();
          streamStatusDiv.classList.add("no-data");
        }

        getContainer().appendChild(streamStatusDiv);
      }
    }

    function removeStreamStatusDiv() {
      var streamStatusDiv = getContainer().querySelector('.stream-status');

      if (streamStatusDiv) {
        streamStatusDiv.parentNode.removeChild(streamStatusDiv);
      }
    }

    function handleSessionOnline() {
      statechangeCallback({
        state: "available",
        info: "The configured stream is available."
      });
      streamIsUnavailable = false;
      removeStreamStatusDiv();
    }

    function getContainer() {
      return player.element.parentNode.parentNode;
    }

    function getSrc(sourceDescription) {
      return sourceDescription.sources[0].src;
    } // pass along back-ups streams to other sourcedescriptions
    // function getBackupStreams(sourceDescription) {
    //     return sourceDescription.metadata.session && sourceDescription.metadata.session.backupStreams;
    // }


    function streamInterval() {
      var sources = [player.source];

      if (sessionSource.backupStreams && sessionSource.backupStreams.length > 0) {
        for (var i = 0; i < sessionSource.backupStreams.length; i++) {
          sources.push(sessionSource.backupStreams[i]);
        }
      }

      console.log("sources", sources);

      var f = (function(){
        var xhttp = [], i;
        for (var i = 0; i < sources.length; i++) {
          var currentSrc = getSrc(sources[i]);
          var testSrc = currentSrc;
          var rn = Math.round(Math.random() * 1000000000000);

          if (currentSrc.indexOf("?") > -1) {
            testSrc = currentSrc + "&rn=" + rn;
          } else {
            testSrc = currentSrc + "?rn=" + rn;
          }

          (function(i){
            xhttp[i] = new XMLHttpRequest();
            xhttp[i].open("GET", testSrc, true);
            xhttp[i].onreadystatechange = function(){
              if (xhttp[i].readyState === 4 && xhttp[i].status !== 200 && isWaiting && !streamIsUnavailable) {
                handleSessionOffline();
              } else if (xhttp[i].readyState === 4 && xhttp[i].status === 200 && streamIsUnavailable) {
                handleSessionOnline();
                reloadCurrentStream(sources[i], i);
              }
            };
            xhttp[i].send();
          })(i);
        }
      })();
    }

    function checkStreamStatus() {
      isWaiting = true;

      if (!hasInterval) {
        hasInterval = true;
        sessionStreamInterval = setInterval(streamInterval, getRetryTimeout());
      }
    }

    function requestInterceptor(request) {
      if (request.url == player.src) {
        // check master playlist
        var urlSplit = request.url.split("?");

        if (urlSplit.length > 1) {
          // check if master playlist has query params
          queryParameter = urlSplit[1];
        } else {
          queryParameter = null; // reset if no query params
        }
      } else if (queryParameter) {
        // if no master playlist and active set of query params
        var newUrl = request.url + "?" + queryParameter; // append query params

        request.redirect({
          url: newUrl
        });
      }
    }

    function setPlaying() {
      isWaiting = false;
    }

    function registerSessionEvents() {
      if (player) {
        player.network.addEventListener('offline', handleSessionOffline);
        player.addEventListener('error', handleSessionOffline);
        player.network.addEventListener('online', handleSessionOnline);
        player.addEventListener('waiting', checkStreamStatus);
        player.addEventListener('playing', setPlaying);

        //if (sessionSource && sessionSource.queryParametersPassthrough) {
        //  player.network.addRequestInterceptor(requestInterceptor);
        //}
      }
    }

    function unregisterSessionEvents() {
      if (player) {
        sessionData = null;
        sessionSource = null;
        streamIsUnavailable = false;
        clearInterval(sessionStreamInterval);
        clearInterval(countdownTimerInterval);
        isWaiting = false;
        hasInterval = false;
        statechangeCallback = null;
        datachangeCallback = null;
        player.network.removeEventListener('offline', handleSessionOffline);
        player.removeEventListener('error', handleSessionOffline);
        player.network.removeEventListener('online', handleSessionOnline);
        player.removeEventListener('waiting', checkStreamStatus);
        player.removeEventListener('playing', setPlaying);
        player.network.addRequestInterceptor(requestInterceptor);
      }
    }

    function getPoster() {
      return sessionData && sessionData.image || player.source.metadata && player.source.metadata.session && player.source.metadata.session.placeholderImageUrl;
    }

    function sourceHandler(e) {
      unregisterSessionEvents();
      removeStreamStatusDiv();
      sessionSource = e.source.metadata && e.source.metadata.session;

      if (sessionSource) {
        registerSessionEvents();
        //var sessionJson = sessionSource.jsonUrl;

        //if (sessionJson) {
        //  registerSessionData(sessionJson);
        //}

        statechangeCallback = sessionSource.statechangeCallback;
        datachangeCallback = sessionSource.datachangeCallback;
      }
    }

/*    function registerSessionData(url) {
      var xhttp = new XMLHttpRequest();

      xhttp.onreadystatechange = function () {
        if (this.readyState == 4 && this.status == 200) {
          sessionData = JSON.parse(xhttp.responseText);
          datachangeCallback({
            data: sessionData,
            status: "available"
          });
          player.poster = getPoster();
        }
      };

      xhttp.open("GET", url, true);
      xhttp.send();
    }*/

    function reloadCurrentStream(source, i) {
      player.autoplay = true;
      if (source) {
        if (source.metadata.session.backupStreams) {
           source = source;
        } else {
          var backupStreams = player.source.metadata.session.backupStreams;
          var originalSourceFixedFormatting = fixSource(player.source);
          backupStreams[i-1] = originalSourceFixedFormatting;
          source.metadata.session.backupStreams = backupStreams;
        }
        player.source = source;
      } else {
        player.source = player.source;
      }
    }

    function fixSource(source) {
      if (source.metadata.session.backupStreams) {
        delete source.metadata.session.backupStreams;
      }
      return source;
    }

    function getSessionData() {
      return sessionData;
    }

    function getState() {
      return streamIsUnavailable ? "offline" : "online";
    }

    player.addEventListener('sourcechange', sourceHandler);
    return {
      getStreamData: getSessionData,
      getState: getState
    };
  }
};
window.theoplayerSession = theoplayerSession;