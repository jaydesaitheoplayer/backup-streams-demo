/******/ (function(modules) { // webpackBootstrap
/******/    // The module cache
/******/    var installedModules = {};
/******/
/******/    // The require function
/******/    function __webpack_require__(moduleId) {
/******/
/******/        // Check if module is in cache
/******/        if(installedModules[moduleId]) {
/******/            return installedModules[moduleId].exports;
/******/        }
/******/        // Create a new module (and put it into the cache)
/******/        var module = installedModules[moduleId] = {
/******/            i: moduleId,
/******/            l: false,
/******/            exports: {}
/******/        };
/******/
/******/        // Execute the module function
/******/        modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/        // Flag the module as loaded
/******/        module.l = true;
/******/
/******/        // Return the exports of the module
/******/        return module.exports;
/******/    }
/******/
/******/
/******/    // expose the modules object (__webpack_modules__)
/******/    __webpack_require__.m = modules;
/******/
/******/    // expose the module cache
/******/    __webpack_require__.c = installedModules;
/******/
/******/    // define getter function for harmony exports
/******/    __webpack_require__.d = function(exports, name, getter) {
/******/        if(!__webpack_require__.o(exports, name)) {
/******/            Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/        }
/******/    };
/******/
/******/    // define __esModule on exports
/******/    __webpack_require__.r = function(exports) {
/******/        if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/            Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/        }
/******/        Object.defineProperty(exports, '__esModule', { value: true });
/******/    };
/******/
/******/    // create a fake namespace object
/******/    // mode & 1: value is a module id, require it
/******/    // mode & 2: merge all properties of value into the ns
/******/    // mode & 4: return value when already ns object
/******/    // mode & 8|1: behave like require
/******/    __webpack_require__.t = function(value, mode) {
/******/        if(mode & 1) value = __webpack_require__(value);
/******/        if(mode & 8) return value;
/******/        if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/        var ns = Object.create(null);
/******/        __webpack_require__.r(ns);
/******/        Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/        if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/        return ns;
/******/    };
/******/
/******/    // getDefaultExport function for compatibility with non-harmony modules
/******/    __webpack_require__.n = function(module) {
/******/        var getter = module && module.__esModule ?
/******/            function getDefault() { return module['default']; } :
/******/            function getModuleExports() { return module; };
/******/        __webpack_require__.d(getter, 'a', getter);
/******/        return getter;
/******/    };
/******/
/******/    // Object.prototype.hasOwnProperty.call
/******/    __webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/    // __webpack_public_path__
/******/    __webpack_require__.p = "";
/******/
/******/
/******/    // Load entry module and return exports
/******/    return __webpack_require__(__webpack_require__.s = "./src/index.js");
/******/ })
/************************************************************************/
/******/ ({

/***/ "./src/index.js":
/*!**********************!*\
  !*** ./src/index.js ***!
  \**********************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

var theoplayerSession = {
  registerPlayer: function registerPlayer(player) {
    var sessionData = null;
    var sessionSource = null;
    var streamIsUnavailable = false;
    var sessionStreamInterval;
    var countdownTimerInterval;
    var isWaiting = false;
    var hasInterval = false;
    var statechangeCallback = null;
    var datachangeCallback = null;
    var queryParameter = null;

    function handleSessionOffline() {
      statechangeCallback({
        state: "unavailable",
        info: "The configured stream is currently unavailable."
      });
      streamIsUnavailable = true;
      createStreamStatusDiv();

      if (!hasInterval) {
        hasInterval = true;
        sessionStreamInterval = setInterval(streamInterval, getRetryTimeout());
      }
    }

    function getRetryTimeout() {
      return sessionSource && sessionSource.retry || 3000;
    }

    function getOfflineText() {
      return sessionSource && sessionSource.offlineText || "The stream is currently not available.";
    }

    function setupCountdown(countDownDate) {
      var countDownDate = countDownDate.getTime();
      countdownTimerInterval = setInterval(function () {
        var now = new Date().getTime();
        var distance = countDownDate - now;
        var days = Math.floor(distance / (1000 * 60 * 60 * 24));
        var hours = Math.floor(distance % (1000 * 60 * 60 * 24) / (1000 * 60 * 60));
        var minutes = Math.floor(distance % (1000 * 60 * 60) / (1000 * 60));
        var seconds = Math.floor(distance % (1000 * 60) / 1000);
        getContainer().querySelector('.countdown').innerHTML = days + "d " + hours + "h " + minutes + "m " + seconds + "s ";

        if (distance < 0) {
          clearInterval(countdownTimerInterval);
          getContainer().querySelector('.countdown').innerHTML = "Starting...";
        }
      }, 1000);
    }

    function createStreamStatusDiv() {
      var streamStatusDiv = getContainer().querySelector('.stream-status');

      if (!streamStatusDiv) {
        streamStatusDiv = document.createElement('div');
        streamStatusDiv.className = 'stream-status';
        var poster = getPoster();

        if (poster) {
          streamStatusDiv.style.background = 'url("' + poster + '")';
          streamStatusDiv.style.backgroundSize = 'cover';
        }

        if (sessionData) {
          var countdown = sessionData['countdown_timestamp'];

          if (countdown) {
            countdown = new Date(parseInt(countdown) * 1000);

            if (countdown.getTime() > Date.now()) {
              // in the future
              var countdownDiv = document.createElement('div');
              countdownDiv.className = "stream-status-overlay";
              var date = countdown.toDateString();
              var hhmm = countdown.toISOString().substr(11, 5);
              var title = sessionData['title'] + "<br />" || false;
              countdownDiv.innerHTML = title + "Stream will start on:<br />" + date + " @ " + hhmm;
              streamStatusDiv.appendChild(countdownDiv);
              var countdownTimer = document.createElement('div');
              countdownTimer.className = 'countdown';
              countdownTimer.innerHTML = "&nbsp;";
              countdownDiv.appendChild(countdownTimer);
              setupCountdown(countdown);
            }
          }
        } else {
          streamStatusDiv.textContent = getOfflineText();
          streamStatusDiv.classList.add("no-data");
        }

        getContainer().appendChild(streamStatusDiv);
      }
    }

    function removeStreamStatusDiv() {
      var streamStatusDiv = getContainer().querySelector('.stream-status');

      if (streamStatusDiv) {
        streamStatusDiv.parentNode.removeChild(streamStatusDiv);
      }
    }

    function handleSessionOnline() {
      statechangeCallback({
        state: "available",
        info: "The configured stream is available."
      });
      streamIsUnavailable = false;
      removeStreamStatusDiv();
    }

    function getContainer() {
      return player.element.parentNode.parentNode;
    }

    function getSrc(sourceDescription) {
      return sourceDescription.sources[0].src;
    } // pass along back-ups streams to other sourcedescriptions
    // function getBackupStreams(sourceDescription) {
    //     return sourceDescription.metadata.session && sourceDescription.metadata.session.backupStreams;
    // }


    function streamInterval() {
      var sources = [player.source];

      if (sessionSource.backupStreams && sessionSource.backupStreams.length > 0) {
        for (var i = 0; i < sessionSource.backupStreams.length; i++) {
          sources.push(sessionSource.backupStreams[i]);
        }
      }

      console.log("sources", sources);

      var f = (function(){
        var xhttp = [], i;
        for (var i = 0; i < sources.length; i++) {
          var currentSrc = getSrc(sources[i]);
          var testSrc = currentSrc;
          var rn = Math.round(Math.random() * 1000000000000);

          if (currentSrc.indexOf("?") > -1) {
            testSrc = currentSrc + "&rn=" + rn;
          } else {
            testSrc = currentSrc + "?rn=" + rn;
          }

          (function(i){
            xhttp[i] = new XMLHttpRequest();
            xhttp[i].open("GET", testSrc, true);
            xhttp[i].onreadystatechange = function(){
              if (xhttp[i].readyState === 4 && xhttp[i].status !== 200 && isWaiting && !streamIsUnavailable) {
                handleSessionOffline();
              } else if (xhttp[i].readyState === 4 && xhttp[i].status === 200 && streamIsUnavailable) {
                handleSessionOnline();
                reloadCurrentStream(sources[i], i);
              }
            };
            xhttp[i].send();
          })(i);
        }
      })();
    }

    function checkStreamStatus() {
      isWaiting = true;

      if (!hasInterval) {
        hasInterval = true;
        sessionStreamInterval = setInterval(streamInterval, getRetryTimeout());
      }
    }

    function requestInterceptor(request) {
      if (request.url == player.src) {
        // check master playlist
        var urlSplit = request.url.split("?");

        if (urlSplit.length > 1) {
          // check if master playlist has query params
          queryParameter = urlSplit[1];
        } else {
          queryParameter = null; // reset if no query params
        }
      } else if (queryParameter) {
        // if no master playlist and active set of query params
        var newUrl = request.url + "?" + queryParameter; // append query params

        request.redirect({
          url: newUrl
        });
      }
    }

    function setPlaying() {
      isWaiting = false;
    }

    function registerSessionEvents() {
      if (player) {
        player.network.addEventListener('offline', handleSessionOffline);
        player.addEventListener('error', handleSessionOffline);
        player.network.addEventListener('online', handleSessionOnline);
        player.addEventListener('waiting', checkStreamStatus);
        player.addEventListener('playing', setPlaying);

        //if (sessionSource && sessionSource.queryParametersPassthrough) {
        //  player.network.addRequestInterceptor(requestInterceptor);
        //}
      }
    }

    function unregisterSessionEvents() {
      if (player) {
        sessionData = null;
        sessionSource = null;
        streamIsUnavailable = false;
        clearInterval(sessionStreamInterval);
        clearInterval(countdownTimerInterval);
        isWaiting = false;
        hasInterval = false;
        statechangeCallback = null;
        datachangeCallback = null;
        player.network.removeEventListener('offline', handleSessionOffline);
        player.removeEventListener('error', handleSessionOffline);
        player.network.removeEventListener('online', handleSessionOnline);
        player.removeEventListener('waiting', checkStreamStatus);
        player.removeEventListener('playing', setPlaying);
        player.network.addRequestInterceptor(requestInterceptor);
      }
    }

    function getPoster() {
      return sessionData && sessionData.image || player.source.metadata && player.source.metadata.session && player.source.metadata.session.placeholderImageUrl;
    }

    function sourceHandler(e) {
      unregisterSessionEvents();
      removeStreamStatusDiv();
      sessionSource = e.source.metadata && e.source.metadata.session;

      if (sessionSource) {
        registerSessionEvents();
        //var sessionJson = sessionSource.jsonUrl;

        //if (sessionJson) {
        //  registerSessionData(sessionJson);
        //}

        statechangeCallback = sessionSource.statechangeCallback;
        datachangeCallback = sessionSource.datachangeCallback;
      }
    }

/*    function registerSessionData(url) {
      var xhttp = new XMLHttpRequest();

      xhttp.onreadystatechange = function () {
        if (this.readyState == 4 && this.status == 200) {
          sessionData = JSON.parse(xhttp.responseText);
          datachangeCallback({
            data: sessionData,
            status: "available"
          });
          player.poster = getPoster();
        }
      };

      xhttp.open("GET", url, true);
      xhttp.send();
    }*/

    function reloadCurrentStream(source, i) {
      player.autoplay = true;
      if (source) {
        if (source.metadata.session.backupStreams) {
           source = source;
        } else {
          var backupStreams = player.source.metadata.session.backupStreams;
          var originalSourceFixedFormatting = fixSource(player.source);
          backupStreams[i-1] = originalSourceFixedFormatting;
          source.metadata.session.backupStreams = backupStreams;
        }
        player.source = source;
      } else {
        player.source = player.source;
      }
    }

    function fixSource(source) {
      if (source.metadata.session.backupStreams) {
        delete source.metadata.session.backupStreams;
      }
      return source;
    }

    function getSessionData() {
      return sessionData;
    }

    function getState() {
      return streamIsUnavailable ? "offline" : "online";
    }

    player.addEventListener('sourcechange', sourceHandler);
    return {
      getStreamData: getSessionData,
      getState: getState
    };
  }
};
window.theoplayerSession = theoplayerSession;

/***/ })

/******/ });
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8vd2VicGFjay9ib290c3RyYXAiLCJ3ZWJwYWNrOi8vLy4vc3JjL2luZGV4LmpzIl0sIm5hbWVzIjpbInRoZW9wbGF5ZXJXb3d6YSIsInJlZ2lzdGVyUGxheWVyIiwicGxheWVyIiwid293emFEYXRhIiwid293emFTb3VyY2UiLCJzdHJlYW1Jc1VuYXZhaWxhYmxlIiwid293emFTdHJlYW1JbnRlcnZhbCIsImNvdW50ZG93blRpbWVySW50ZXJ2YWwiLCJpc1dhaXRpbmciLCJoYXNJbnRlcnZhbCIsInN0YXRlY2hhbmdlQ2FsbGJhY2siLCJkYXRhY2hhbmdlQ2FsbGJhY2siLCJxdWVyeVBhcmFtZXRlciIsImhhbmRsZVdvd3phT2ZmbGluZSIsInN0YXRlIiwiaW5mbyIsImNyZWF0ZVN0cmVhbVN0YXR1c0RpdiIsInNldEludGVydmFsIiwic3RyZWFtSW50ZXJ2YWwiLCJnZXRSZXRyeVRpbWVvdXQiLCJyZXRyeSIsImdldE9mZmxpbmVUZXh0Iiwib2ZmbGluZVRleHQiLCJzZXR1cENvdW50ZG93biIsImNvdW50RG93bkRhdGUiLCJnZXRUaW1lIiwibm93IiwiRGF0ZSIsImRpc3RhbmNlIiwiZGF5cyIsIk1hdGgiLCJmbG9vciIsImhvdXJzIiwibWludXRlcyIsInNlY29uZHMiLCJnZXRDb250YWluZXIiLCJxdWVyeVNlbGVjdG9yIiwiaW5uZXJIVE1MIiwiY2xlYXJJbnRlcnZhbCIsInN0cmVhbVN0YXR1c0RpdiIsImRvY3VtZW50IiwiY3JlYXRlRWxlbWVudCIsImNsYXNzTmFtZSIsInBvc3RlciIsImdldFBvc3RlciIsInN0eWxlIiwiYmFja2dyb3VuZCIsImJhY2tncm91bmRTaXplIiwiY291bnRkb3duIiwicGFyc2VJbnQiLCJjb3VudGRvd25EaXYiLCJkYXRlIiwidG9EYXRlU3RyaW5nIiwiaGhtbSIsInRvSVNPU3RyaW5nIiwic3Vic3RyIiwidGl0bGUiLCJhcHBlbmRDaGlsZCIsImNvdW50ZG93blRpbWVyIiwidGV4dENvbnRlbnQiLCJjbGFzc0xpc3QiLCJhZGQiLCJyZW1vdmVTdHJlYW1TdGF0dXNEaXYiLCJwYXJlbnROb2RlIiwicmVtb3ZlQ2hpbGQiLCJoYW5kbGVXb3d6YU9ubGluZSIsImVsZW1lbnQiLCJnZXRTcmMiLCJzb3VyY2VEZXNjcmlwdGlvbiIsInNvdXJjZXMiLCJzcmMiLCJzb3VyY2UiLCJiYWNrdXBTdHJlYW1zIiwibGVuZ3RoIiwiaSIsInB1c2giLCJjb25zb2xlIiwibG9nIiwiY3VycmVudFNyYyIsInRlc3RTcmMiLCJybiIsInJvdW5kIiwicmFuZG9tIiwiaW5kZXhPZiIsInhodHRwIiwiWE1MSHR0cFJlcXVlc3QiLCJvbnJlYWR5c3RhdGVjaGFuZ2UiLCJyZWFkeVN0YXRlIiwic3RhdHVzIiwicmVsb2FkQ3VycmVudFN0cmVhbSIsIm9wZW4iLCJzZW5kIiwiY2hlY2tTdHJlYW1TdGF0dXMiLCJyZXF1ZXN0SW50ZXJjZXB0b3IiLCJyZXF1ZXN0IiwidXJsIiwidXJsU3BsaXQiLCJzcGxpdCIsIm5ld1VybCIsInJlZGlyZWN0Iiwic2V0UGxheWluZyIsInJlZ2lzdGVyV293emFFdmVudHMiLCJuZXR3b3JrIiwiYWRkRXZlbnRMaXN0ZW5lciIsInF1ZXJ5UGFyYW1ldGVyc1Bhc3N0aHJvdWdoIiwiYWRkUmVxdWVzdEludGVyY2VwdG9yIiwidW5yZWdpc3Rlcldvd3phRXZlbnRzIiwicmVtb3ZlRXZlbnRMaXN0ZW5lciIsImltYWdlIiwibWV0YWRhdGEiLCJ3b3d6YSIsInBsYWNlaG9sZGVySW1hZ2VVcmwiLCJzb3VyY2VIYW5kbGVyIiwiZSIsIndvd3phSnNvbiIsImpzb25VcmwiLCJyZWdpc3Rlcldvd3phRGF0YSIsIkpTT04iLCJwYXJzZSIsInJlc3BvbnNlVGV4dCIsImRhdGEiLCJhdXRvcGxheSIsImdldFdvd3phRGF0YSIsImdldFN0YXRlIiwiZ2V0U3RyZWFtRGF0YSIsIndpbmRvdyJdLCJtYXBwaW5ncyI6IjtRQUFBO1FBQ0E7O1FBRUE7UUFDQTs7UUFFQTtRQUNBO1FBQ0E7UUFDQTtRQUNBO1FBQ0E7UUFDQTtRQUNBO1FBQ0E7UUFDQTs7UUFFQTtRQUNBOztRQUVBO1FBQ0E7O1FBRUE7UUFDQTtRQUNBOzs7UUFHQTtRQUNBOztRQUVBO1FBQ0E7O1FBRUE7UUFDQTtRQUNBO1FBQ0EsMENBQTBDLGdDQUFnQztRQUMxRTtRQUNBOztRQUVBO1FBQ0E7UUFDQTtRQUNBLHdEQUF3RCxrQkFBa0I7UUFDMUU7UUFDQSxpREFBaUQsY0FBYztRQUMvRDs7UUFFQTtRQUNBO1FBQ0E7UUFDQTtRQUNBO1FBQ0E7UUFDQTtRQUNBO1FBQ0E7UUFDQTtRQUNBO1FBQ0EseUNBQXlDLGlDQUFpQztRQUMxRSxnSEFBZ0gsbUJBQW1CLEVBQUU7UUFDckk7UUFDQTs7UUFFQTtRQUNBO1FBQ0E7UUFDQSwyQkFBMkIsMEJBQTBCLEVBQUU7UUFDdkQsaUNBQWlDLGVBQWU7UUFDaEQ7UUFDQTtRQUNBOztRQUVBO1FBQ0Esc0RBQXNELCtEQUErRDs7UUFFckg7UUFDQTs7O1FBR0E7UUFDQTs7Ozs7Ozs7Ozs7O0FDbEZBLElBQUlBLGVBQWUsR0FBRztBQUNsQkMsZ0JBQWMsRUFBRSx3QkFBU0MsTUFBVCxFQUFpQjtBQUU3QixRQUFJQyxTQUFTLEdBQUcsSUFBaEI7QUFDQSxRQUFJQyxXQUFXLEdBQUcsSUFBbEI7QUFDQSxRQUFJQyxtQkFBbUIsR0FBRyxLQUExQjtBQUNBLFFBQUlDLG1CQUFKO0FBQ0EsUUFBSUMsc0JBQUo7QUFDQSxRQUFJQyxTQUFTLEdBQUcsS0FBaEI7QUFDQSxRQUFJQyxXQUFXLEdBQUcsS0FBbEI7QUFDQSxRQUFJQyxtQkFBbUIsR0FBRyxJQUExQjtBQUNBLFFBQUlDLGtCQUFrQixHQUFHLElBQXpCO0FBQ0EsUUFBSUMsY0FBYyxHQUFHLElBQXJCOztBQUVBLGFBQVNDLGtCQUFULEdBQThCO0FBQzFCSCx5QkFBbUIsQ0FBQztBQUFDSSxhQUFLLEVBQUUsYUFBUjtBQUF1QkMsWUFBSSxFQUFFO0FBQTdCLE9BQUQsQ0FBbkI7QUFDQVYseUJBQW1CLEdBQUcsSUFBdEI7QUFDQVcsMkJBQXFCOztBQUNyQixVQUFJLENBQUNQLFdBQUwsRUFBa0I7QUFDZEEsbUJBQVcsR0FBRyxJQUFkO0FBQ0FILDJCQUFtQixHQUFHVyxXQUFXLENBQUNDLGNBQUQsRUFBaUJDLGVBQWUsRUFBaEMsQ0FBakM7QUFDSDtBQUNKOztBQUVELGFBQVNBLGVBQVQsR0FBMkI7QUFDdkIsYUFBU2YsV0FBVyxJQUFJQSxXQUFXLENBQUNnQixLQUE1QixJQUFzQyxJQUE5QztBQUNIOztBQUVELGFBQVNDLGNBQVQsR0FBMEI7QUFDdEIsYUFBU2pCLFdBQVcsSUFBSUEsV0FBVyxDQUFDa0IsV0FBNUIsSUFBNEMsd0NBQXBEO0FBQ0g7O0FBRUQsYUFBU0MsY0FBVCxDQUF3QkMsYUFBeEIsRUFBdUM7QUFDbkMsVUFBSUEsYUFBYSxHQUFHQSxhQUFhLENBQUNDLE9BQWQsRUFBcEI7QUFDQWxCLDRCQUFzQixHQUFHVSxXQUFXLENBQUMsWUFBVztBQUM1QyxZQUFNUyxHQUFHLEdBQUcsSUFBSUMsSUFBSixHQUFXRixPQUFYLEVBQVo7QUFDQSxZQUFNRyxRQUFRLEdBQUdKLGFBQWEsR0FBR0UsR0FBakM7QUFDQSxZQUFNRyxJQUFJLEdBQUdDLElBQUksQ0FBQ0MsS0FBTCxDQUFXSCxRQUFRLElBQUksT0FBTyxFQUFQLEdBQVksRUFBWixHQUFpQixFQUFyQixDQUFuQixDQUFiO0FBQ0EsWUFBTUksS0FBSyxHQUFHRixJQUFJLENBQUNDLEtBQUwsQ0FBWUgsUUFBUSxJQUFJLE9BQU8sRUFBUCxHQUFZLEVBQVosR0FBaUIsRUFBckIsQ0FBVCxJQUFzQyxPQUFPLEVBQVAsR0FBWSxFQUFsRCxDQUFYLENBQWQ7QUFDQSxZQUFNSyxPQUFPLEdBQUdILElBQUksQ0FBQ0MsS0FBTCxDQUFZSCxRQUFRLElBQUksT0FBTyxFQUFQLEdBQVksRUFBaEIsQ0FBVCxJQUFpQyxPQUFPLEVBQXhDLENBQVgsQ0FBaEI7QUFDQSxZQUFNTSxPQUFPLEdBQUdKLElBQUksQ0FBQ0MsS0FBTCxDQUFZSCxRQUFRLElBQUksT0FBTyxFQUFYLENBQVQsR0FBMkIsSUFBdEMsQ0FBaEI7QUFDQU8sb0JBQVksR0FBR0MsYUFBZixDQUE2QixZQUE3QixFQUEyQ0MsU0FBM0MsR0FBdURSLElBQUksR0FBRyxJQUFQLEdBQWNHLEtBQWQsR0FBc0IsSUFBdEIsR0FDakRDLE9BRGlELEdBQ3ZDLElBRHVDLEdBQ2hDQyxPQURnQyxHQUN0QixJQURqQzs7QUFFQSxZQUFJTixRQUFRLEdBQUcsQ0FBZixFQUFrQjtBQUNkVSx1QkFBYSxDQUFDL0Isc0JBQUQsQ0FBYjtBQUNBNEIsc0JBQVksR0FBR0MsYUFBZixDQUE2QixZQUE3QixFQUEyQ0MsU0FBM0MsR0FBdUQsYUFBdkQ7QUFDSDtBQUNKLE9BYm1DLEVBYWpDLElBYmlDLENBQXBDO0FBY0g7O0FBRUQsYUFBU3JCLHFCQUFULEdBQWlDO0FBQzdCLFVBQUl1QixlQUFlLEdBQUdKLFlBQVksR0FBR0MsYUFBZixDQUE2QixnQkFBN0IsQ0FBdEI7O0FBQ0EsVUFBSSxDQUFDRyxlQUFMLEVBQXNCO0FBQ2xCQSx1QkFBZSxHQUFHQyxRQUFRLENBQUNDLGFBQVQsQ0FBdUIsS0FBdkIsQ0FBbEI7QUFDQUYsdUJBQWUsQ0FBQ0csU0FBaEIsR0FBNEIsZUFBNUI7QUFDQSxZQUFJQyxNQUFNLEdBQUdDLFNBQVMsRUFBdEI7O0FBQ0EsWUFBSUQsTUFBSixFQUFZO0FBQ1JKLHlCQUFlLENBQUNNLEtBQWhCLENBQXNCQyxVQUF0QixHQUFtQyxVQUFVSCxNQUFWLEdBQW1CLElBQXREO0FBQ0FKLHlCQUFlLENBQUNNLEtBQWhCLENBQXNCRSxjQUF0QixHQUF1QyxPQUF2QztBQUNIOztBQUVELFlBQUk1QyxTQUFKLEVBQWU7QUFDWCxjQUFJNkMsU0FBUyxHQUFHN0MsU0FBUyxDQUFDLHFCQUFELENBQXpCOztBQUNBLGNBQUk2QyxTQUFKLEVBQWU7QUFDWEEscUJBQVMsR0FBRyxJQUFJckIsSUFBSixDQUFVc0IsUUFBUSxDQUFDRCxTQUFELENBQVIsR0FBb0IsSUFBOUIsQ0FBWjs7QUFDQSxnQkFBSUEsU0FBUyxDQUFDdkIsT0FBVixLQUFzQkUsSUFBSSxDQUFDRCxHQUFMLEVBQTFCLEVBQXNDO0FBQUU7QUFDcEMsa0JBQUl3QixZQUFZLEdBQUdWLFFBQVEsQ0FBQ0MsYUFBVCxDQUF1QixLQUF2QixDQUFuQjtBQUNBUywwQkFBWSxDQUFDUixTQUFiLEdBQXlCLHVCQUF6QjtBQUNBLGtCQUFJUyxJQUFJLEdBQUdILFNBQVMsQ0FBQ0ksWUFBVixFQUFYO0FBQ0Esa0JBQUlDLElBQUksR0FBR0wsU0FBUyxDQUFDTSxXQUFWLEdBQXdCQyxNQUF4QixDQUErQixFQUEvQixFQUFtQyxDQUFuQyxDQUFYO0FBQ0Esa0JBQUlDLEtBQUssR0FBR3JELFNBQVMsQ0FBQyxPQUFELENBQVQsR0FBcUIsUUFBckIsSUFBaUMsS0FBN0M7QUFDQStDLDBCQUFZLENBQUNiLFNBQWIsR0FBeUJtQixLQUFLLEdBQUcsNkJBQVIsR0FBd0NMLElBQXhDLEdBQStDLEtBQS9DLEdBQXVERSxJQUFoRjtBQUNBZCw2QkFBZSxDQUFDa0IsV0FBaEIsQ0FBNEJQLFlBQTVCO0FBRUEsa0JBQUlRLGNBQWMsR0FBR2xCLFFBQVEsQ0FBQ0MsYUFBVCxDQUF1QixLQUF2QixDQUFyQjtBQUNBaUIsNEJBQWMsQ0FBQ2hCLFNBQWYsR0FBMkIsV0FBM0I7QUFDQWdCLDRCQUFjLENBQUNyQixTQUFmLEdBQTJCLFFBQTNCO0FBQ0FhLDBCQUFZLENBQUNPLFdBQWIsQ0FBeUJDLGNBQXpCO0FBQ0FuQyw0QkFBYyxDQUFDeUIsU0FBRCxDQUFkO0FBQ0g7QUFDSjtBQUNKLFNBcEJELE1BcUJLO0FBQ0RULHlCQUFlLENBQUNvQixXQUFoQixHQUE4QnRDLGNBQWMsRUFBNUM7QUFDQWtCLHlCQUFlLENBQUNxQixTQUFoQixDQUEwQkMsR0FBMUIsQ0FBOEIsU0FBOUI7QUFDSDs7QUFFRDFCLG9CQUFZLEdBQUdzQixXQUFmLENBQTJCbEIsZUFBM0I7QUFDSDtBQUNKOztBQUNELGFBQVN1QixxQkFBVCxHQUFpQztBQUM3QixVQUFNdkIsZUFBZSxHQUFHSixZQUFZLEdBQUdDLGFBQWYsQ0FBNkIsZ0JBQTdCLENBQXhCOztBQUNBLFVBQUlHLGVBQUosRUFBcUI7QUFDakJBLHVCQUFlLENBQUN3QixVQUFoQixDQUEyQkMsV0FBM0IsQ0FBdUN6QixlQUF2QztBQUNIO0FBQ0o7O0FBQ0QsYUFBUzBCLGlCQUFULEdBQTZCO0FBQ3pCdkQseUJBQW1CLENBQUM7QUFBQ0ksYUFBSyxFQUFFLFdBQVI7QUFBcUJDLFlBQUksRUFBRTtBQUEzQixPQUFELENBQW5CO0FBQ0FWLHlCQUFtQixHQUFHLEtBQXRCO0FBQ0F5RCwyQkFBcUI7QUFDeEI7O0FBRUQsYUFBUzNCLFlBQVQsR0FBd0I7QUFDcEIsYUFBT2pDLE1BQU0sQ0FBQ2dFLE9BQVAsQ0FBZUgsVUFBZixDQUEwQkEsVUFBakM7QUFDSDs7QUFFRCxhQUFTSSxNQUFULENBQWdCQyxpQkFBaEIsRUFBbUM7QUFDL0IsYUFBT0EsaUJBQWlCLENBQUNDLE9BQWxCLENBQTBCLENBQTFCLEVBQTZCQyxHQUFwQztBQUNILEtBM0c0QixDQTZHN0I7QUFDQTtBQUNBO0FBQ0E7OztBQUVBLGFBQVNwRCxjQUFULEdBQTBCO0FBQ3RCLFVBQU1tRCxPQUFPLEdBQUcsQ0FBQ25FLE1BQU0sQ0FBQ3FFLE1BQVIsQ0FBaEI7O0FBQ0EsVUFBSW5FLFdBQVcsQ0FBQ29FLGFBQVosSUFBNkJwRSxXQUFXLENBQUNvRSxhQUFaLENBQTBCQyxNQUExQixHQUFtQyxDQUFwRSxFQUF1RTtBQUNuRSxhQUFLLElBQUlDLENBQUMsR0FBRyxDQUFiLEVBQWdCQSxDQUFDLEdBQUd0RSxXQUFXLENBQUNvRSxhQUFaLENBQTBCQyxNQUE5QyxFQUFzREMsQ0FBQyxFQUF2RCxFQUEyRDtBQUN2REwsaUJBQU8sQ0FBQ00sSUFBUixDQUFhdkUsV0FBVyxDQUFDb0UsYUFBWixDQUEwQkUsQ0FBMUIsQ0FBYjtBQUNIO0FBQ0o7O0FBQ0RFLGFBQU8sQ0FBQ0MsR0FBUixDQUFZLFNBQVosRUFBdUJSLE9BQXZCOztBQUNBLFdBQUssSUFBSUssQ0FBQyxHQUFHLENBQWIsRUFBZ0JBLENBQUMsR0FBR0wsT0FBTyxDQUFDSSxNQUE1QixFQUFvQ0MsQ0FBQyxFQUFyQyxFQUF5QztBQUNyQyxZQUFNSSxVQUFVLEdBQUdYLE1BQU0sQ0FBQ0UsT0FBTyxDQUFDSyxDQUFELENBQVIsQ0FBekI7QUFDQSxZQUFJSyxPQUFPLEdBQUdELFVBQWQ7QUFDQSxZQUFNRSxFQUFFLEdBQUdsRCxJQUFJLENBQUNtRCxLQUFMLENBQVduRCxJQUFJLENBQUNvRCxNQUFMLEtBQWdCLGFBQTNCLENBQVg7O0FBQ0EsWUFBSUosVUFBVSxDQUFDSyxPQUFYLENBQW1CLEdBQW5CLElBQTBCLENBQUMsQ0FBL0IsRUFBa0M7QUFDOUJKLGlCQUFPLEdBQUdELFVBQVUsR0FBRyxNQUFiLEdBQXNCRSxFQUFoQztBQUNILFNBRkQsTUFFTztBQUNIRCxpQkFBTyxHQUFHRCxVQUFVLEdBQUcsTUFBYixHQUFzQkUsRUFBaEM7QUFDSDs7QUFDRCxZQUFNSSxLQUFLLEdBQUcsSUFBSUMsY0FBSixFQUFkOztBQUNBRCxhQUFLLENBQUNFLGtCQUFOLEdBQTJCLFlBQVk7QUFDbkMsY0FBSSxLQUFLQyxVQUFMLElBQW1CLENBQW5CLElBQXlCLEtBQUtDLE1BQUwsSUFBZSxHQUF4QyxJQUFnRGhGLFNBQWhELElBQTZELENBQUNILG1CQUFsRSxFQUF1RjtBQUNuRlEsOEJBQWtCO0FBQ3JCLFdBRkQsTUFFTyxJQUFJLEtBQUswRSxVQUFMLElBQW1CLENBQW5CLElBQXlCLEtBQUtDLE1BQUwsSUFBZSxHQUF4QyxJQUFnRG5GLG1CQUFwRCxFQUF5RTtBQUM1RTRELDZCQUFpQjtBQUNqQndCLCtCQUFtQixDQUFDcEIsT0FBTyxDQUFDSyxDQUFELENBQVIsQ0FBbkI7QUFDSDtBQUNKLFNBUEQ7O0FBUUFVLGFBQUssQ0FBQ00sSUFBTixDQUFXLEtBQVgsRUFBa0JYLE9BQWxCLEVBQTJCLElBQTNCO0FBQ0FLLGFBQUssQ0FBQ08sSUFBTjtBQUNIO0FBQ0o7O0FBRUQsYUFBU0MsaUJBQVQsR0FBNkI7QUFDekJwRixlQUFTLEdBQUcsSUFBWjs7QUFDQSxVQUFJLENBQUNDLFdBQUwsRUFBa0I7QUFDZEEsbUJBQVcsR0FBRyxJQUFkO0FBQ0FILDJCQUFtQixHQUFHVyxXQUFXLENBQUNDLGNBQUQsRUFBaUJDLGVBQWUsRUFBaEMsQ0FBakM7QUFDSDtBQUNKOztBQUVELGFBQVMwRSxrQkFBVCxDQUE0QkMsT0FBNUIsRUFBcUM7QUFDakMsVUFBSUEsT0FBTyxDQUFDQyxHQUFSLElBQWU3RixNQUFNLENBQUNvRSxHQUExQixFQUErQjtBQUFFO0FBQzdCLFlBQU0wQixRQUFRLEdBQUdGLE9BQU8sQ0FBQ0MsR0FBUixDQUFZRSxLQUFaLENBQWtCLEdBQWxCLENBQWpCOztBQUNBLFlBQUlELFFBQVEsQ0FBQ3ZCLE1BQVQsR0FBa0IsQ0FBdEIsRUFBeUI7QUFBRTtBQUN2QjdELHdCQUFjLEdBQUdvRixRQUFRLENBQUMsQ0FBRCxDQUF6QjtBQUNILFNBRkQsTUFFTztBQUNIcEYsd0JBQWMsR0FBRyxJQUFqQixDQURHLENBQ29CO0FBQzFCO0FBQ0osT0FQRCxNQU9PLElBQUlBLGNBQUosRUFBb0I7QUFBRTtBQUN6QixZQUFNc0YsTUFBTSxHQUFHSixPQUFPLENBQUNDLEdBQVIsR0FBYyxHQUFkLEdBQW9CbkYsY0FBbkMsQ0FEdUIsQ0FDNEI7O0FBQ25Ea0YsZUFBTyxDQUFDSyxRQUFSLENBQWlCO0FBQ2JKLGFBQUcsRUFBRUc7QUFEUSxTQUFqQjtBQUdIO0FBQ0o7O0FBRUQsYUFBU0UsVUFBVCxHQUFzQjtBQUNsQjVGLGVBQVMsR0FBRyxLQUFaO0FBQ0g7O0FBRUQsYUFBUzZGLG1CQUFULEdBQStCO0FBQzNCLFVBQUluRyxNQUFKLEVBQVk7QUFDUkEsY0FBTSxDQUFDb0csT0FBUCxDQUFlQyxnQkFBZixDQUFnQyxTQUFoQyxFQUEyQzFGLGtCQUEzQztBQUNBWCxjQUFNLENBQUNxRyxnQkFBUCxDQUF3QixPQUF4QixFQUFpQzFGLGtCQUFqQztBQUNBWCxjQUFNLENBQUNvRyxPQUFQLENBQWVDLGdCQUFmLENBQWdDLFFBQWhDLEVBQTBDdEMsaUJBQTFDO0FBQ0EvRCxjQUFNLENBQUNxRyxnQkFBUCxDQUF3QixTQUF4QixFQUFtQ1gsaUJBQW5DO0FBQ0ExRixjQUFNLENBQUNxRyxnQkFBUCxDQUF3QixTQUF4QixFQUFtQ0gsVUFBbkM7O0FBQ0EsWUFBSWhHLFdBQVcsSUFBSUEsV0FBVyxDQUFDb0csMEJBQS9CLEVBQTJEO0FBQ3ZEdEcsZ0JBQU0sQ0FBQ29HLE9BQVAsQ0FBZUcscUJBQWYsQ0FBcUNaLGtCQUFyQztBQUNIO0FBQ0o7QUFDSjs7QUFFRCxhQUFTYSxxQkFBVCxHQUFpQztBQUM3QixVQUFJeEcsTUFBSixFQUFZO0FBQ1JDLGlCQUFTLEdBQUcsSUFBWjtBQUNBQyxtQkFBVyxHQUFHLElBQWQ7QUFDQUMsMkJBQW1CLEdBQUcsS0FBdEI7QUFDQWlDLHFCQUFhLENBQUNoQyxtQkFBRCxDQUFiO0FBQ0FnQyxxQkFBYSxDQUFDL0Isc0JBQUQsQ0FBYjtBQUNBQyxpQkFBUyxHQUFHLEtBQVo7QUFDQUMsbUJBQVcsR0FBRyxLQUFkO0FBQ0FDLDJCQUFtQixHQUFHLElBQXRCO0FBQ0FDLDBCQUFrQixHQUFHLElBQXJCO0FBQ0FULGNBQU0sQ0FBQ29HLE9BQVAsQ0FBZUssbUJBQWYsQ0FBbUMsU0FBbkMsRUFBOEM5RixrQkFBOUM7QUFDQVgsY0FBTSxDQUFDeUcsbUJBQVAsQ0FBMkIsT0FBM0IsRUFBb0M5RixrQkFBcEM7QUFDQVgsY0FBTSxDQUFDb0csT0FBUCxDQUFlSyxtQkFBZixDQUFtQyxRQUFuQyxFQUE2QzFDLGlCQUE3QztBQUNBL0QsY0FBTSxDQUFDeUcsbUJBQVAsQ0FBMkIsU0FBM0IsRUFBc0NmLGlCQUF0QztBQUNBMUYsY0FBTSxDQUFDeUcsbUJBQVAsQ0FBMkIsU0FBM0IsRUFBc0NQLFVBQXRDO0FBQ0FsRyxjQUFNLENBQUNvRyxPQUFQLENBQWVHLHFCQUFmLENBQXFDWixrQkFBckM7QUFDSDtBQUNKOztBQUVELGFBQVNqRCxTQUFULEdBQXFCO0FBQ2pCLGFBQVN6QyxTQUFTLElBQUlBLFNBQVMsQ0FBQ3lHLEtBQXhCLElBQWtDMUcsTUFBTSxDQUFDcUUsTUFBUCxDQUFjc0MsUUFBZCxJQUEwQjNHLE1BQU0sQ0FBQ3FFLE1BQVAsQ0FBY3NDLFFBQWQsQ0FBdUJDLEtBQWpELElBQTBENUcsTUFBTSxDQUFDcUUsTUFBUCxDQUFjc0MsUUFBZCxDQUF1QkMsS0FBdkIsQ0FBNkJDLG1CQUFqSTtBQUNIOztBQUVELGFBQVNDLGFBQVQsQ0FBdUJDLENBQXZCLEVBQTBCO0FBQ3RCUCwyQkFBcUI7QUFDckI1QywyQkFBcUI7QUFDckIxRCxpQkFBVyxHQUFHNkcsQ0FBQyxDQUFDMUMsTUFBRixDQUFTc0MsUUFBVCxJQUFxQkksQ0FBQyxDQUFDMUMsTUFBRixDQUFTc0MsUUFBVCxDQUFrQkMsS0FBckQ7O0FBQ0EsVUFBSTFHLFdBQUosRUFBaUI7QUFDYmlHLDJCQUFtQjtBQUNuQixZQUFNYSxTQUFTLEdBQUc5RyxXQUFXLENBQUMrRyxPQUE5Qjs7QUFDQSxZQUFJRCxTQUFKLEVBQWU7QUFDWEUsMkJBQWlCLENBQUNGLFNBQUQsQ0FBakI7QUFDSDs7QUFDRHhHLDJCQUFtQixHQUFHTixXQUFXLENBQUNNLG1CQUFsQztBQUNBQywwQkFBa0IsR0FBR1AsV0FBVyxDQUFDTyxrQkFBakM7QUFDSDtBQUNKOztBQUVELGFBQVN5RyxpQkFBVCxDQUEyQnJCLEdBQTNCLEVBQWdDO0FBQzVCLFVBQU1YLEtBQUssR0FBRyxJQUFJQyxjQUFKLEVBQWQ7O0FBQ0FELFdBQUssQ0FBQ0Usa0JBQU4sR0FBMkIsWUFBVztBQUNsQyxZQUFJLEtBQUtDLFVBQUwsSUFBbUIsQ0FBbkIsSUFBeUIsS0FBS0MsTUFBTCxJQUFlLEdBQTVDLEVBQWtEO0FBQzlDckYsbUJBQVMsR0FBR2tILElBQUksQ0FBQ0MsS0FBTCxDQUFXbEMsS0FBSyxDQUFDbUMsWUFBakIsQ0FBWjtBQUNBNUcsNEJBQWtCLENBQUM7QUFBQzZHLGdCQUFJLEVBQUVySCxTQUFQO0FBQWtCcUYsa0JBQU0sRUFBRTtBQUExQixXQUFELENBQWxCO0FBQ0F0RixnQkFBTSxDQUFDeUMsTUFBUCxHQUFnQkMsU0FBUyxFQUF6QjtBQUNIO0FBQ0osT0FORDs7QUFPQXdDLFdBQUssQ0FBQ00sSUFBTixDQUFXLEtBQVgsRUFBa0JLLEdBQWxCLEVBQXVCLElBQXZCO0FBQ0FYLFdBQUssQ0FBQ08sSUFBTjtBQUNIOztBQUVELGFBQVNGLG1CQUFULENBQTZCbEIsTUFBN0IsRUFBcUM7QUFDakNyRSxZQUFNLENBQUN1SCxRQUFQLEdBQWtCLElBQWxCLENBRGlDLENBRWpDO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBQ0EsVUFBSWxELE1BQUosRUFBWTtBQUNSckUsY0FBTSxDQUFDcUUsTUFBUCxHQUFnQkEsTUFBaEI7QUFDSCxPQUZELE1BRU87QUFDSHJFLGNBQU0sQ0FBQ3FFLE1BQVAsR0FBZ0JyRSxNQUFNLENBQUNxRSxNQUF2QjtBQUNIO0FBQ0o7O0FBRUQsYUFBU21ELFlBQVQsR0FBd0I7QUFDcEIsYUFBT3ZILFNBQVA7QUFDSDs7QUFFRCxhQUFTd0gsUUFBVCxHQUFvQjtBQUNoQixhQUFRdEgsbUJBQW1CLEdBQUcsU0FBSCxHQUFlLFFBQTFDO0FBQ0g7O0FBRURILFVBQU0sQ0FBQ3FHLGdCQUFQLENBQXdCLGNBQXhCLEVBQXdDUyxhQUF4QztBQUVBLFdBQU87QUFDSFksbUJBQWEsRUFBRUYsWUFEWjtBQUVIQyxjQUFRLEVBQUVBO0FBRlAsS0FBUDtBQUlIO0FBM1FpQixDQUF0QjtBQTZRQUUsTUFBTSxDQUFDN0gsZUFBUCxHQUF5QkEsZUFBekIsQyIsImZpbGUiOiJ0aGVvcGxheWVyLXdvd3phLmpzIiwic291cmNlc0NvbnRlbnQiOlsiIFx0Ly8gVGhlIG1vZHVsZSBjYWNoZVxuIFx0dmFyIGluc3RhbGxlZE1vZHVsZXMgPSB7fTtcblxuIFx0Ly8gVGhlIHJlcXVpcmUgZnVuY3Rpb25cbiBcdGZ1bmN0aW9uIF9fd2VicGFja19yZXF1aXJlX18obW9kdWxlSWQpIHtcblxuIFx0XHQvLyBDaGVjayBpZiBtb2R1bGUgaXMgaW4gY2FjaGVcbiBcdFx0aWYoaW5zdGFsbGVkTW9kdWxlc1ttb2R1bGVJZF0pIHtcbiBcdFx0XHRyZXR1cm4gaW5zdGFsbGVkTW9kdWxlc1ttb2R1bGVJZF0uZXhwb3J0cztcbiBcdFx0fVxuIFx0XHQvLyBDcmVhdGUgYSBuZXcgbW9kdWxlIChhbmQgcHV0IGl0IGludG8gdGhlIGNhY2hlKVxuIFx0XHR2YXIgbW9kdWxlID0gaW5zdGFsbGVkTW9kdWxlc1ttb2R1bGVJZF0gPSB7XG4gXHRcdFx0aTogbW9kdWxlSWQsXG4gXHRcdFx0bDogZmFsc2UsXG4gXHRcdFx0ZXhwb3J0czoge31cbiBcdFx0fTtcblxuIFx0XHQvLyBFeGVjdXRlIHRoZSBtb2R1bGUgZnVuY3Rpb25cbiBcdFx0bW9kdWxlc1ttb2R1bGVJZF0uY2FsbChtb2R1bGUuZXhwb3J0cywgbW9kdWxlLCBtb2R1bGUuZXhwb3J0cywgX193ZWJwYWNrX3JlcXVpcmVfXyk7XG5cbiBcdFx0Ly8gRmxhZyB0aGUgbW9kdWxlIGFzIGxvYWRlZFxuIFx0XHRtb2R1bGUubCA9IHRydWU7XG5cbiBcdFx0Ly8gUmV0dXJuIHRoZSBleHBvcnRzIG9mIHRoZSBtb2R1bGVcbiBcdFx0cmV0dXJuIG1vZHVsZS5leHBvcnRzO1xuIFx0fVxuXG5cbiBcdC8vIGV4cG9zZSB0aGUgbW9kdWxlcyBvYmplY3QgKF9fd2VicGFja19tb2R1bGVzX18pXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLm0gPSBtb2R1bGVzO1xuXG4gXHQvLyBleHBvc2UgdGhlIG1vZHVsZSBjYWNoZVxuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5jID0gaW5zdGFsbGVkTW9kdWxlcztcblxuIFx0Ly8gZGVmaW5lIGdldHRlciBmdW5jdGlvbiBmb3IgaGFybW9ueSBleHBvcnRzXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLmQgPSBmdW5jdGlvbihleHBvcnRzLCBuYW1lLCBnZXR0ZXIpIHtcbiBcdFx0aWYoIV9fd2VicGFja19yZXF1aXJlX18ubyhleHBvcnRzLCBuYW1lKSkge1xuIFx0XHRcdE9iamVjdC5kZWZpbmVQcm9wZXJ0eShleHBvcnRzLCBuYW1lLCB7IGVudW1lcmFibGU6IHRydWUsIGdldDogZ2V0dGVyIH0pO1xuIFx0XHR9XG4gXHR9O1xuXG4gXHQvLyBkZWZpbmUgX19lc01vZHVsZSBvbiBleHBvcnRzXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLnIgPSBmdW5jdGlvbihleHBvcnRzKSB7XG4gXHRcdGlmKHR5cGVvZiBTeW1ib2wgIT09ICd1bmRlZmluZWQnICYmIFN5bWJvbC50b1N0cmluZ1RhZykge1xuIFx0XHRcdE9iamVjdC5kZWZpbmVQcm9wZXJ0eShleHBvcnRzLCBTeW1ib2wudG9TdHJpbmdUYWcsIHsgdmFsdWU6ICdNb2R1bGUnIH0pO1xuIFx0XHR9XG4gXHRcdE9iamVjdC5kZWZpbmVQcm9wZXJ0eShleHBvcnRzLCAnX19lc01vZHVsZScsIHsgdmFsdWU6IHRydWUgfSk7XG4gXHR9O1xuXG4gXHQvLyBjcmVhdGUgYSBmYWtlIG5hbWVzcGFjZSBvYmplY3RcbiBcdC8vIG1vZGUgJiAxOiB2YWx1ZSBpcyBhIG1vZHVsZSBpZCwgcmVxdWlyZSBpdFxuIFx0Ly8gbW9kZSAmIDI6IG1lcmdlIGFsbCBwcm9wZXJ0aWVzIG9mIHZhbHVlIGludG8gdGhlIG5zXG4gXHQvLyBtb2RlICYgNDogcmV0dXJuIHZhbHVlIHdoZW4gYWxyZWFkeSBucyBvYmplY3RcbiBcdC8vIG1vZGUgJiA4fDE6IGJlaGF2ZSBsaWtlIHJlcXVpcmVcbiBcdF9fd2VicGFja19yZXF1aXJlX18udCA9IGZ1bmN0aW9uKHZhbHVlLCBtb2RlKSB7XG4gXHRcdGlmKG1vZGUgJiAxKSB2YWx1ZSA9IF9fd2VicGFja19yZXF1aXJlX18odmFsdWUpO1xuIFx0XHRpZihtb2RlICYgOCkgcmV0dXJuIHZhbHVlO1xuIFx0XHRpZigobW9kZSAmIDQpICYmIHR5cGVvZiB2YWx1ZSA9PT0gJ29iamVjdCcgJiYgdmFsdWUgJiYgdmFsdWUuX19lc01vZHVsZSkgcmV0dXJuIHZhbHVlO1xuIFx0XHR2YXIgbnMgPSBPYmplY3QuY3JlYXRlKG51bGwpO1xuIFx0XHRfX3dlYnBhY2tfcmVxdWlyZV9fLnIobnMpO1xuIFx0XHRPYmplY3QuZGVmaW5lUHJvcGVydHkobnMsICdkZWZhdWx0JywgeyBlbnVtZXJhYmxlOiB0cnVlLCB2YWx1ZTogdmFsdWUgfSk7XG4gXHRcdGlmKG1vZGUgJiAyICYmIHR5cGVvZiB2YWx1ZSAhPSAnc3RyaW5nJykgZm9yKHZhciBrZXkgaW4gdmFsdWUpIF9fd2VicGFja19yZXF1aXJlX18uZChucywga2V5LCBmdW5jdGlvbihrZXkpIHsgcmV0dXJuIHZhbHVlW2tleV07IH0uYmluZChudWxsLCBrZXkpKTtcbiBcdFx0cmV0dXJuIG5zO1xuIFx0fTtcblxuIFx0Ly8gZ2V0RGVmYXVsdEV4cG9ydCBmdW5jdGlvbiBmb3IgY29tcGF0aWJpbGl0eSB3aXRoIG5vbi1oYXJtb255IG1vZHVsZXNcbiBcdF9fd2VicGFja19yZXF1aXJlX18ubiA9IGZ1bmN0aW9uKG1vZHVsZSkge1xuIFx0XHR2YXIgZ2V0dGVyID0gbW9kdWxlICYmIG1vZHVsZS5fX2VzTW9kdWxlID9cbiBcdFx0XHRmdW5jdGlvbiBnZXREZWZhdWx0KCkgeyByZXR1cm4gbW9kdWxlWydkZWZhdWx0J107IH0gOlxuIFx0XHRcdGZ1bmN0aW9uIGdldE1vZHVsZUV4cG9ydHMoKSB7IHJldHVybiBtb2R1bGU7IH07XG4gXHRcdF9fd2VicGFja19yZXF1aXJlX18uZChnZXR0ZXIsICdhJywgZ2V0dGVyKTtcbiBcdFx0cmV0dXJuIGdldHRlcjtcbiBcdH07XG5cbiBcdC8vIE9iamVjdC5wcm90b3R5cGUuaGFzT3duUHJvcGVydHkuY2FsbFxuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5vID0gZnVuY3Rpb24ob2JqZWN0LCBwcm9wZXJ0eSkgeyByZXR1cm4gT2JqZWN0LnByb3RvdHlwZS5oYXNPd25Qcm9wZXJ0eS5jYWxsKG9iamVjdCwgcHJvcGVydHkpOyB9O1xuXG4gXHQvLyBfX3dlYnBhY2tfcHVibGljX3BhdGhfX1xuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5wID0gXCJcIjtcblxuXG4gXHQvLyBMb2FkIGVudHJ5IG1vZHVsZSBhbmQgcmV0dXJuIGV4cG9ydHNcbiBcdHJldHVybiBfX3dlYnBhY2tfcmVxdWlyZV9fKF9fd2VicGFja19yZXF1aXJlX18ucyA9IFwiLi9zcmMvaW5kZXguanNcIik7XG4iLCJ2YXIgdGhlb3BsYXllcldvd3phID0ge1xuICAgIHJlZ2lzdGVyUGxheWVyOiBmdW5jdGlvbihwbGF5ZXIpIHtcblxuICAgICAgICBsZXQgd293emFEYXRhID0gbnVsbDtcbiAgICAgICAgbGV0IHdvd3phU291cmNlID0gbnVsbDtcbiAgICAgICAgbGV0IHN0cmVhbUlzVW5hdmFpbGFibGUgPSBmYWxzZTtcbiAgICAgICAgbGV0IHdvd3phU3RyZWFtSW50ZXJ2YWw7XG4gICAgICAgIGxldCBjb3VudGRvd25UaW1lckludGVydmFsO1xuICAgICAgICBsZXQgaXNXYWl0aW5nID0gZmFsc2U7XG4gICAgICAgIGxldCBoYXNJbnRlcnZhbCA9IGZhbHNlO1xuICAgICAgICBsZXQgc3RhdGVjaGFuZ2VDYWxsYmFjayA9IG51bGw7XG4gICAgICAgIGxldCBkYXRhY2hhbmdlQ2FsbGJhY2sgPSBudWxsO1xuICAgICAgICBsZXQgcXVlcnlQYXJhbWV0ZXIgPSBudWxsO1xuXG4gICAgICAgIGZ1bmN0aW9uIGhhbmRsZVdvd3phT2ZmbGluZSgpIHtcbiAgICAgICAgICAgIHN0YXRlY2hhbmdlQ2FsbGJhY2soe3N0YXRlOiBcInVuYXZhaWxhYmxlXCIsIGluZm86IFwiVGhlIGNvbmZpZ3VyZWQgc3RyZWFtIGlzIGN1cnJlbnRseSB1bmF2YWlsYWJsZS5cIn0pO1xuICAgICAgICAgICAgc3RyZWFtSXNVbmF2YWlsYWJsZSA9IHRydWU7XG4gICAgICAgICAgICBjcmVhdGVTdHJlYW1TdGF0dXNEaXYoKTtcbiAgICAgICAgICAgIGlmICghaGFzSW50ZXJ2YWwpIHtcbiAgICAgICAgICAgICAgICBoYXNJbnRlcnZhbCA9IHRydWU7XG4gICAgICAgICAgICAgICAgd293emFTdHJlYW1JbnRlcnZhbCA9IHNldEludGVydmFsKHN0cmVhbUludGVydmFsLCBnZXRSZXRyeVRpbWVvdXQoKSk7XG4gICAgICAgICAgICB9XG4gICAgICAgIH1cblxuICAgICAgICBmdW5jdGlvbiBnZXRSZXRyeVRpbWVvdXQoKSB7XG4gICAgICAgICAgICByZXR1cm4gKCh3b3d6YVNvdXJjZSAmJiB3b3d6YVNvdXJjZS5yZXRyeSkgfHwgMzAwMCk7XG4gICAgICAgIH1cblxuICAgICAgICBmdW5jdGlvbiBnZXRPZmZsaW5lVGV4dCgpIHtcbiAgICAgICAgICAgIHJldHVybiAoKHdvd3phU291cmNlICYmIHdvd3phU291cmNlLm9mZmxpbmVUZXh0KSB8fCBcIlRoZSBzdHJlYW0gaXMgY3VycmVudGx5IG5vdCBhdmFpbGFibGUuXCIpO1xuICAgICAgICB9XG5cbiAgICAgICAgZnVuY3Rpb24gc2V0dXBDb3VudGRvd24oY291bnREb3duRGF0ZSkge1xuICAgICAgICAgICAgdmFyIGNvdW50RG93bkRhdGUgPSBjb3VudERvd25EYXRlLmdldFRpbWUoKTtcbiAgICAgICAgICAgIGNvdW50ZG93blRpbWVySW50ZXJ2YWwgPSBzZXRJbnRlcnZhbChmdW5jdGlvbigpIHtcbiAgICAgICAgICAgICAgICBjb25zdCBub3cgPSBuZXcgRGF0ZSgpLmdldFRpbWUoKTtcbiAgICAgICAgICAgICAgICBjb25zdCBkaXN0YW5jZSA9IGNvdW50RG93bkRhdGUgLSBub3c7XG4gICAgICAgICAgICAgICAgY29uc3QgZGF5cyA9IE1hdGguZmxvb3IoZGlzdGFuY2UgLyAoMTAwMCAqIDYwICogNjAgKiAyNCkpO1xuICAgICAgICAgICAgICAgIGNvbnN0IGhvdXJzID0gTWF0aC5mbG9vcigoZGlzdGFuY2UgJSAoMTAwMCAqIDYwICogNjAgKiAyNCkpIC8gKDEwMDAgKiA2MCAqIDYwKSk7XG4gICAgICAgICAgICAgICAgY29uc3QgbWludXRlcyA9IE1hdGguZmxvb3IoKGRpc3RhbmNlICUgKDEwMDAgKiA2MCAqIDYwKSkgLyAoMTAwMCAqIDYwKSk7XG4gICAgICAgICAgICAgICAgY29uc3Qgc2Vjb25kcyA9IE1hdGguZmxvb3IoKGRpc3RhbmNlICUgKDEwMDAgKiA2MCkpIC8gMTAwMCk7XG4gICAgICAgICAgICAgICAgZ2V0Q29udGFpbmVyKCkucXVlcnlTZWxlY3RvcignLmNvdW50ZG93bicpLmlubmVySFRNTCA9IGRheXMgKyBcImQgXCIgKyBob3VycyArIFwiaCBcIlxuICAgICAgICAgICAgICAgICAgICArIG1pbnV0ZXMgKyBcIm0gXCIgKyBzZWNvbmRzICsgXCJzIFwiO1xuICAgICAgICAgICAgICAgIGlmIChkaXN0YW5jZSA8IDApIHtcbiAgICAgICAgICAgICAgICAgICAgY2xlYXJJbnRlcnZhbChjb3VudGRvd25UaW1lckludGVydmFsKTtcbiAgICAgICAgICAgICAgICAgICAgZ2V0Q29udGFpbmVyKCkucXVlcnlTZWxlY3RvcignLmNvdW50ZG93bicpLmlubmVySFRNTCA9IFwiU3RhcnRpbmcuLi5cIjtcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICB9LCAxMDAwKTtcbiAgICAgICAgfVxuXG4gICAgICAgIGZ1bmN0aW9uIGNyZWF0ZVN0cmVhbVN0YXR1c0RpdigpIHtcbiAgICAgICAgICAgIGxldCBzdHJlYW1TdGF0dXNEaXYgPSBnZXRDb250YWluZXIoKS5xdWVyeVNlbGVjdG9yKCcuc3RyZWFtLXN0YXR1cycpO1xuICAgICAgICAgICAgaWYgKCFzdHJlYW1TdGF0dXNEaXYpIHtcbiAgICAgICAgICAgICAgICBzdHJlYW1TdGF0dXNEaXYgPSBkb2N1bWVudC5jcmVhdGVFbGVtZW50KCdkaXYnKTtcbiAgICAgICAgICAgICAgICBzdHJlYW1TdGF0dXNEaXYuY2xhc3NOYW1lID0gJ3N0cmVhbS1zdGF0dXMnO1xuICAgICAgICAgICAgICAgIGxldCBwb3N0ZXIgPSBnZXRQb3N0ZXIoKTtcbiAgICAgICAgICAgICAgICBpZiAocG9zdGVyKSB7XG4gICAgICAgICAgICAgICAgICAgIHN0cmVhbVN0YXR1c0Rpdi5zdHlsZS5iYWNrZ3JvdW5kID0gJ3VybChcIicgKyBwb3N0ZXIgKyAnXCIpJztcbiAgICAgICAgICAgICAgICAgICAgc3RyZWFtU3RhdHVzRGl2LnN0eWxlLmJhY2tncm91bmRTaXplID0gJ2NvdmVyJztcbiAgICAgICAgICAgICAgICB9XG5cbiAgICAgICAgICAgICAgICBpZiAod293emFEYXRhKSB7XG4gICAgICAgICAgICAgICAgICAgIGxldCBjb3VudGRvd24gPSB3b3d6YURhdGFbJ2NvdW50ZG93bl90aW1lc3RhbXAnXTtcbiAgICAgICAgICAgICAgICAgICAgaWYgKGNvdW50ZG93bikge1xuICAgICAgICAgICAgICAgICAgICAgICAgY291bnRkb3duID0gbmV3IERhdGUoKHBhcnNlSW50KGNvdW50ZG93bikqMTAwMCkpO1xuICAgICAgICAgICAgICAgICAgICAgICAgaWYgKGNvdW50ZG93bi5nZXRUaW1lKCkgPiBEYXRlLm5vdygpKSB7IC8vIGluIHRoZSBmdXR1cmVcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBsZXQgY291bnRkb3duRGl2ID0gZG9jdW1lbnQuY3JlYXRlRWxlbWVudCgnZGl2Jyk7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgY291bnRkb3duRGl2LmNsYXNzTmFtZSA9IFwic3RyZWFtLXN0YXR1cy1vdmVybGF5XCI7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgbGV0IGRhdGUgPSBjb3VudGRvd24udG9EYXRlU3RyaW5nKCk7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgbGV0IGhobW0gPSBjb3VudGRvd24udG9JU09TdHJpbmcoKS5zdWJzdHIoMTEsIDUpO1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGxldCB0aXRsZSA9IHdvd3phRGF0YVsndGl0bGUnXSArIFwiPGJyIC8+XCIgfHwgXCJcIjtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBjb3VudGRvd25EaXYuaW5uZXJIVE1MID0gdGl0bGUgKyBcIlN0cmVhbSB3aWxsIHN0YXJ0IG9uOjxiciAvPlwiICsgZGF0ZSArIFwiIEAgXCIgKyBoaG1tO1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHN0cmVhbVN0YXR1c0Rpdi5hcHBlbmRDaGlsZChjb3VudGRvd25EaXYpO1xuXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgbGV0IGNvdW50ZG93blRpbWVyID0gZG9jdW1lbnQuY3JlYXRlRWxlbWVudCgnZGl2Jyk7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgY291bnRkb3duVGltZXIuY2xhc3NOYW1lID0gJ2NvdW50ZG93bic7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgY291bnRkb3duVGltZXIuaW5uZXJIVE1MID0gXCImbmJzcDtcIjtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBjb3VudGRvd25EaXYuYXBwZW5kQ2hpbGQoY291bnRkb3duVGltZXIpO1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHNldHVwQ291bnRkb3duKGNvdW50ZG93bik7XG4gICAgICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgZWxzZSB7XG4gICAgICAgICAgICAgICAgICAgIHN0cmVhbVN0YXR1c0Rpdi50ZXh0Q29udGVudCA9IGdldE9mZmxpbmVUZXh0KCk7XG4gICAgICAgICAgICAgICAgICAgIHN0cmVhbVN0YXR1c0Rpdi5jbGFzc0xpc3QuYWRkKFwibm8tZGF0YVwiKTtcbiAgICAgICAgICAgICAgICB9XG5cbiAgICAgICAgICAgICAgICBnZXRDb250YWluZXIoKS5hcHBlbmRDaGlsZChzdHJlYW1TdGF0dXNEaXYpO1xuICAgICAgICAgICAgfVxuICAgICAgICB9XG4gICAgICAgIGZ1bmN0aW9uIHJlbW92ZVN0cmVhbVN0YXR1c0RpdigpIHtcbiAgICAgICAgICAgIGNvbnN0IHN0cmVhbVN0YXR1c0RpdiA9IGdldENvbnRhaW5lcigpLnF1ZXJ5U2VsZWN0b3IoJy5zdHJlYW0tc3RhdHVzJyk7XG4gICAgICAgICAgICBpZiAoc3RyZWFtU3RhdHVzRGl2KSB7XG4gICAgICAgICAgICAgICAgc3RyZWFtU3RhdHVzRGl2LnBhcmVudE5vZGUucmVtb3ZlQ2hpbGQoc3RyZWFtU3RhdHVzRGl2KTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgfVxuICAgICAgICBmdW5jdGlvbiBoYW5kbGVXb3d6YU9ubGluZSgpIHtcbiAgICAgICAgICAgIHN0YXRlY2hhbmdlQ2FsbGJhY2soe3N0YXRlOiBcImF2YWlsYWJsZVwiLCBpbmZvOiBcIlRoZSBjb25maWd1cmVkIHN0cmVhbSBpcyBhdmFpbGFibGUuXCJ9KVxuICAgICAgICAgICAgc3RyZWFtSXNVbmF2YWlsYWJsZSA9IGZhbHNlO1xuICAgICAgICAgICAgcmVtb3ZlU3RyZWFtU3RhdHVzRGl2KCk7XG4gICAgICAgIH1cblxuICAgICAgICBmdW5jdGlvbiBnZXRDb250YWluZXIoKSB7XG4gICAgICAgICAgICByZXR1cm4gcGxheWVyLmVsZW1lbnQucGFyZW50Tm9kZS5wYXJlbnROb2RlO1xuICAgICAgICB9XG5cbiAgICAgICAgZnVuY3Rpb24gZ2V0U3JjKHNvdXJjZURlc2NyaXB0aW9uKSB7XG4gICAgICAgICAgICByZXR1cm4gc291cmNlRGVzY3JpcHRpb24uc291cmNlc1swXS5zcmM7XG4gICAgICAgIH1cblxuICAgICAgICAvLyBwYXNzIGFsb25nIGJhY2stdXBzIHN0cmVhbXMgdG8gb3RoZXIgc291cmNlZGVzY3JpcHRpb25zXG4gICAgICAgIC8vIGZ1bmN0aW9uIGdldEJhY2t1cFN0cmVhbXMoc291cmNlRGVzY3JpcHRpb24pIHtcbiAgICAgICAgLy8gICAgIHJldHVybiBzb3VyY2VEZXNjcmlwdGlvbi5tZXRhZGF0YS53b3d6YSAmJiBzb3VyY2VEZXNjcmlwdGlvbi5tZXRhZGF0YS53b3d6YS5iYWNrdXBTdHJlYW1zO1xuICAgICAgICAvLyB9XG5cbiAgICAgICAgZnVuY3Rpb24gc3RyZWFtSW50ZXJ2YWwoKSB7XG4gICAgICAgICAgICBjb25zdCBzb3VyY2VzID0gW3BsYXllci5zb3VyY2VdO1xuICAgICAgICAgICAgaWYgKHdvd3phU291cmNlLmJhY2t1cFN0cmVhbXMgJiYgd293emFTb3VyY2UuYmFja3VwU3RyZWFtcy5sZW5ndGggPiAwKSB7XG4gICAgICAgICAgICAgICAgZm9yICh2YXIgaSA9IDA7IGkgPCB3b3d6YVNvdXJjZS5iYWNrdXBTdHJlYW1zLmxlbmd0aDsgaSsrKSB7XG4gICAgICAgICAgICAgICAgICAgIHNvdXJjZXMucHVzaCh3b3d6YVNvdXJjZS5iYWNrdXBTdHJlYW1zW2ldKTtcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICB9XG4gICAgICAgICAgICBjb25zb2xlLmxvZyhcInNvdXJjZXNcIiwgc291cmNlcyk7XG4gICAgICAgICAgICBmb3IgKHZhciBpID0gMDsgaSA8IHNvdXJjZXMubGVuZ3RoOyBpKyspIHtcbiAgICAgICAgICAgICAgICBjb25zdCBjdXJyZW50U3JjID0gZ2V0U3JjKHNvdXJjZXNbaV0pO1xuICAgICAgICAgICAgICAgIGxldCB0ZXN0U3JjID0gY3VycmVudFNyYztcbiAgICAgICAgICAgICAgICBjb25zdCBybiA9IE1hdGgucm91bmQoTWF0aC5yYW5kb20oKSAqIDEwMDAwMDAwMDAwMDApO1xuICAgICAgICAgICAgICAgIGlmIChjdXJyZW50U3JjLmluZGV4T2YoXCI/XCIpID4gLTEpIHtcbiAgICAgICAgICAgICAgICAgICAgdGVzdFNyYyA9IGN1cnJlbnRTcmMgKyBcIiZybj1cIiArIHJuO1xuICAgICAgICAgICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICAgICAgICAgIHRlc3RTcmMgPSBjdXJyZW50U3JjICsgXCI/cm49XCIgKyBybjtcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgY29uc3QgeGh0dHAgPSBuZXcgWE1MSHR0cFJlcXVlc3QoKTtcbiAgICAgICAgICAgICAgICB4aHR0cC5vbnJlYWR5c3RhdGVjaGFuZ2UgPSBmdW5jdGlvbiAoKSB7XG4gICAgICAgICAgICAgICAgICAgIGlmICh0aGlzLnJlYWR5U3RhdGUgPT0gNCAmJiAodGhpcy5zdGF0dXMgIT0gMjAwKSAmJiBpc1dhaXRpbmcgJiYgIXN0cmVhbUlzVW5hdmFpbGFibGUpIHtcbiAgICAgICAgICAgICAgICAgICAgICAgIGhhbmRsZVdvd3phT2ZmbGluZSgpO1xuICAgICAgICAgICAgICAgICAgICB9IGVsc2UgaWYgKHRoaXMucmVhZHlTdGF0ZSA9PSA0ICYmICh0aGlzLnN0YXR1cyA9PSAyMDApICYmIHN0cmVhbUlzVW5hdmFpbGFibGUpIHtcbiAgICAgICAgICAgICAgICAgICAgICAgIGhhbmRsZVdvd3phT25saW5lKCk7XG4gICAgICAgICAgICAgICAgICAgICAgICByZWxvYWRDdXJyZW50U3RyZWFtKHNvdXJjZXNbaV0pO1xuICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgfTtcbiAgICAgICAgICAgICAgICB4aHR0cC5vcGVuKFwiR0VUXCIsIHRlc3RTcmMsIHRydWUpO1xuICAgICAgICAgICAgICAgIHhodHRwLnNlbmQoKTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgfVxuXG4gICAgICAgIGZ1bmN0aW9uIGNoZWNrU3RyZWFtU3RhdHVzKCkge1xuICAgICAgICAgICAgaXNXYWl0aW5nID0gdHJ1ZTtcbiAgICAgICAgICAgIGlmICghaGFzSW50ZXJ2YWwpIHtcbiAgICAgICAgICAgICAgICBoYXNJbnRlcnZhbCA9IHRydWU7XG4gICAgICAgICAgICAgICAgd293emFTdHJlYW1JbnRlcnZhbCA9IHNldEludGVydmFsKHN0cmVhbUludGVydmFsLCBnZXRSZXRyeVRpbWVvdXQoKSk7XG4gICAgICAgICAgICB9XG4gICAgICAgIH1cblxuICAgICAgICBmdW5jdGlvbiByZXF1ZXN0SW50ZXJjZXB0b3IocmVxdWVzdCkge1xuICAgICAgICAgICAgaWYgKHJlcXVlc3QudXJsID09IHBsYXllci5zcmMpIHsgLy8gY2hlY2sgbWFzdGVyIHBsYXlsaXN0XG4gICAgICAgICAgICAgICAgY29uc3QgdXJsU3BsaXQgPSByZXF1ZXN0LnVybC5zcGxpdChcIj9cIik7XG4gICAgICAgICAgICAgICAgaWYgKHVybFNwbGl0Lmxlbmd0aCA+IDEpIHsgLy8gY2hlY2sgaWYgbWFzdGVyIHBsYXlsaXN0IGhhcyBxdWVyeSBwYXJhbXNcbiAgICAgICAgICAgICAgICAgICAgcXVlcnlQYXJhbWV0ZXIgPSB1cmxTcGxpdFsxXTtcbiAgICAgICAgICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgICAgICAgICAgICBxdWVyeVBhcmFtZXRlciA9IG51bGw7IC8vIHJlc2V0IGlmIG5vIHF1ZXJ5IHBhcmFtc1xuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgIH0gZWxzZSBpZiAocXVlcnlQYXJhbWV0ZXIpIHsgLy8gaWYgbm8gbWFzdGVyIHBsYXlsaXN0IGFuZCBhY3RpdmUgc2V0IG9mIHF1ZXJ5IHBhcmFtc1xuICAgICAgICAgICAgICAgIGNvbnN0IG5ld1VybCA9IHJlcXVlc3QudXJsICsgXCI/XCIgKyBxdWVyeVBhcmFtZXRlcjsgLy8gYXBwZW5kIHF1ZXJ5IHBhcmFtc1xuICAgICAgICAgICAgICAgIHJlcXVlc3QucmVkaXJlY3Qoe1xuICAgICAgICAgICAgICAgICAgICB1cmw6IG5ld1VybFxuICAgICAgICAgICAgICAgIH0pO1xuICAgICAgICAgICAgfVxuICAgICAgICB9XG5cbiAgICAgICAgZnVuY3Rpb24gc2V0UGxheWluZygpIHtcbiAgICAgICAgICAgIGlzV2FpdGluZyA9IGZhbHNlO1xuICAgICAgICB9XG5cbiAgICAgICAgZnVuY3Rpb24gcmVnaXN0ZXJXb3d6YUV2ZW50cygpIHtcbiAgICAgICAgICAgIGlmIChwbGF5ZXIpIHtcbiAgICAgICAgICAgICAgICBwbGF5ZXIubmV0d29yay5hZGRFdmVudExpc3RlbmVyKCdvZmZsaW5lJywgaGFuZGxlV293emFPZmZsaW5lKTtcbiAgICAgICAgICAgICAgICBwbGF5ZXIuYWRkRXZlbnRMaXN0ZW5lcignZXJyb3InLCBoYW5kbGVXb3d6YU9mZmxpbmUpO1xuICAgICAgICAgICAgICAgIHBsYXllci5uZXR3b3JrLmFkZEV2ZW50TGlzdGVuZXIoJ29ubGluZScsIGhhbmRsZVdvd3phT25saW5lKTtcbiAgICAgICAgICAgICAgICBwbGF5ZXIuYWRkRXZlbnRMaXN0ZW5lcignd2FpdGluZycsIGNoZWNrU3RyZWFtU3RhdHVzKTtcbiAgICAgICAgICAgICAgICBwbGF5ZXIuYWRkRXZlbnRMaXN0ZW5lcigncGxheWluZycsIHNldFBsYXlpbmcpO1xuICAgICAgICAgICAgICAgIGlmICh3b3d6YVNvdXJjZSAmJiB3b3d6YVNvdXJjZS5xdWVyeVBhcmFtZXRlcnNQYXNzdGhyb3VnaCkge1xuICAgICAgICAgICAgICAgICAgICBwbGF5ZXIubmV0d29yay5hZGRSZXF1ZXN0SW50ZXJjZXB0b3IocmVxdWVzdEludGVyY2VwdG9yKTtcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICB9XG4gICAgICAgIH1cblxuICAgICAgICBmdW5jdGlvbiB1bnJlZ2lzdGVyV293emFFdmVudHMoKSB7XG4gICAgICAgICAgICBpZiAocGxheWVyKSB7XG4gICAgICAgICAgICAgICAgd293emFEYXRhID0gbnVsbDtcbiAgICAgICAgICAgICAgICB3b3d6YVNvdXJjZSA9IG51bGw7XG4gICAgICAgICAgICAgICAgc3RyZWFtSXNVbmF2YWlsYWJsZSA9IGZhbHNlO1xuICAgICAgICAgICAgICAgIGNsZWFySW50ZXJ2YWwod293emFTdHJlYW1JbnRlcnZhbCk7XG4gICAgICAgICAgICAgICAgY2xlYXJJbnRlcnZhbChjb3VudGRvd25UaW1lckludGVydmFsKTtcbiAgICAgICAgICAgICAgICBpc1dhaXRpbmcgPSBmYWxzZTtcbiAgICAgICAgICAgICAgICBoYXNJbnRlcnZhbCA9IGZhbHNlO1xuICAgICAgICAgICAgICAgIHN0YXRlY2hhbmdlQ2FsbGJhY2sgPSBudWxsO1xuICAgICAgICAgICAgICAgIGRhdGFjaGFuZ2VDYWxsYmFjayA9IG51bGw7XG4gICAgICAgICAgICAgICAgcGxheWVyLm5ldHdvcmsucmVtb3ZlRXZlbnRMaXN0ZW5lcignb2ZmbGluZScsIGhhbmRsZVdvd3phT2ZmbGluZSk7XG4gICAgICAgICAgICAgICAgcGxheWVyLnJlbW92ZUV2ZW50TGlzdGVuZXIoJ2Vycm9yJywgaGFuZGxlV293emFPZmZsaW5lKTtcbiAgICAgICAgICAgICAgICBwbGF5ZXIubmV0d29yay5yZW1vdmVFdmVudExpc3RlbmVyKCdvbmxpbmUnLCBoYW5kbGVXb3d6YU9ubGluZSk7XG4gICAgICAgICAgICAgICAgcGxheWVyLnJlbW92ZUV2ZW50TGlzdGVuZXIoJ3dhaXRpbmcnLCBjaGVja1N0cmVhbVN0YXR1cyk7XG4gICAgICAgICAgICAgICAgcGxheWVyLnJlbW92ZUV2ZW50TGlzdGVuZXIoJ3BsYXlpbmcnLCBzZXRQbGF5aW5nKVxuICAgICAgICAgICAgICAgIHBsYXllci5uZXR3b3JrLmFkZFJlcXVlc3RJbnRlcmNlcHRvcihyZXF1ZXN0SW50ZXJjZXB0b3IpO1xuICAgICAgICAgICAgfVxuICAgICAgICB9XG5cbiAgICAgICAgZnVuY3Rpb24gZ2V0UG9zdGVyKCkge1xuICAgICAgICAgICAgcmV0dXJuICgod293emFEYXRhICYmIHdvd3phRGF0YS5pbWFnZSkgfHwgcGxheWVyLnNvdXJjZS5tZXRhZGF0YSAmJiBwbGF5ZXIuc291cmNlLm1ldGFkYXRhLndvd3phICYmIHBsYXllci5zb3VyY2UubWV0YWRhdGEud293emEucGxhY2Vob2xkZXJJbWFnZVVybCk7XG4gICAgICAgIH1cblxuICAgICAgICBmdW5jdGlvbiBzb3VyY2VIYW5kbGVyKGUpIHtcbiAgICAgICAgICAgIHVucmVnaXN0ZXJXb3d6YUV2ZW50cygpO1xuICAgICAgICAgICAgcmVtb3ZlU3RyZWFtU3RhdHVzRGl2KCk7XG4gICAgICAgICAgICB3b3d6YVNvdXJjZSA9IGUuc291cmNlLm1ldGFkYXRhICYmIGUuc291cmNlLm1ldGFkYXRhLndvd3phO1xuICAgICAgICAgICAgaWYgKHdvd3phU291cmNlKSB7XG4gICAgICAgICAgICAgICAgcmVnaXN0ZXJXb3d6YUV2ZW50cygpO1xuICAgICAgICAgICAgICAgIGNvbnN0IHdvd3phSnNvbiA9IHdvd3phU291cmNlLmpzb25Vcmw7XG4gICAgICAgICAgICAgICAgaWYgKHdvd3phSnNvbikge1xuICAgICAgICAgICAgICAgICAgICByZWdpc3Rlcldvd3phRGF0YSh3b3d6YUpzb24pO1xuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICBzdGF0ZWNoYW5nZUNhbGxiYWNrID0gd293emFTb3VyY2Uuc3RhdGVjaGFuZ2VDYWxsYmFjaztcbiAgICAgICAgICAgICAgICBkYXRhY2hhbmdlQ2FsbGJhY2sgPSB3b3d6YVNvdXJjZS5kYXRhY2hhbmdlQ2FsbGJhY2s7XG4gICAgICAgICAgICB9XG4gICAgICAgIH1cblxuICAgICAgICBmdW5jdGlvbiByZWdpc3Rlcldvd3phRGF0YSh1cmwpIHtcbiAgICAgICAgICAgIGNvbnN0IHhodHRwID0gbmV3IFhNTEh0dHBSZXF1ZXN0KCk7XG4gICAgICAgICAgICB4aHR0cC5vbnJlYWR5c3RhdGVjaGFuZ2UgPSBmdW5jdGlvbigpIHtcbiAgICAgICAgICAgICAgICBpZiAodGhpcy5yZWFkeVN0YXRlID09IDQgJiYgKHRoaXMuc3RhdHVzID09IDIwMCkpIHtcbiAgICAgICAgICAgICAgICAgICAgd293emFEYXRhID0gSlNPTi5wYXJzZSh4aHR0cC5yZXNwb25zZVRleHQpO1xuICAgICAgICAgICAgICAgICAgICBkYXRhY2hhbmdlQ2FsbGJhY2soe2RhdGE6IHdvd3phRGF0YSwgc3RhdHVzOiBcImF2YWlsYWJsZVwifSk7XG4gICAgICAgICAgICAgICAgICAgIHBsYXllci5wb3N0ZXIgPSBnZXRQb3N0ZXIoKTtcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICB9O1xuICAgICAgICAgICAgeGh0dHAub3BlbihcIkdFVFwiLCB1cmwsIHRydWUpO1xuICAgICAgICAgICAgeGh0dHAuc2VuZCgpO1xuICAgICAgICB9XG5cbiAgICAgICAgZnVuY3Rpb24gcmVsb2FkQ3VycmVudFN0cmVhbShzb3VyY2UpIHtcbiAgICAgICAgICAgIHBsYXllci5hdXRvcGxheSA9IHRydWU7XG4gICAgICAgICAgICAvLyBpZiAoc291cmNlLm1ldGFkYXRhLndvd3phLmJhY2t1cFN0cmVhbXMpIHtcbiAgICAgICAgICAgIC8vICAgICBzb3VyY2UgPSBzb3VyY2U7XG4gICAgICAgICAgICAvLyB9IGVsc2Uge1xuICAgICAgICAgICAgLy8gICAgIHNvdXJjZS5tZXRhZGF0YS53b3d6YS5iYWNrdXBTdHJlYW1zID0gcGxheWVyLnNvdXJjZS5tZXRhZGF0YS53b3d6YS5iYWNrdXBTdHJlYW1zO1xuICAgICAgICAgICAgLy8gfVxuICAgICAgICAgICAgaWYgKHNvdXJjZSkge1xuICAgICAgICAgICAgICAgIHBsYXllci5zb3VyY2UgPSBzb3VyY2U7XG4gICAgICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgICAgICAgIHBsYXllci5zb3VyY2UgPSBwbGF5ZXIuc291cmNlO1xuICAgICAgICAgICAgfVxuICAgICAgICB9XG5cbiAgICAgICAgZnVuY3Rpb24gZ2V0V293emFEYXRhKCkge1xuICAgICAgICAgICAgcmV0dXJuIHdvd3phRGF0YTtcbiAgICAgICAgfVxuXG4gICAgICAgIGZ1bmN0aW9uIGdldFN0YXRlKCkge1xuICAgICAgICAgICAgcmV0dXJuIChzdHJlYW1Jc1VuYXZhaWxhYmxlID8gXCJvZmZsaW5lXCIgOiBcIm9ubGluZVwiKTtcbiAgICAgICAgfVxuXG4gICAgICAgIHBsYXllci5hZGRFdmVudExpc3RlbmVyKCdzb3VyY2VjaGFuZ2UnLCBzb3VyY2VIYW5kbGVyKTtcblxuICAgICAgICByZXR1cm4ge1xuICAgICAgICAgICAgZ2V0U3RyZWFtRGF0YTogZ2V0V293emFEYXRhLFxuICAgICAgICAgICAgZ2V0U3RhdGU6IGdldFN0YXRlXG4gICAgICAgIH07XG4gICAgfVxufVxud2luZG93LnRoZW9wbGF5ZXJXb3d6YSA9IHRoZW9wbGF5ZXJXb3d6YTsiXSwic291cmNlUm9vdCI6IiJ9