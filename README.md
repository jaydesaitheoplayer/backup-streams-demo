# Backup streams with THEOplayer

## Livestream information retrieval
When setting a stream, you can supply one or more backup streams which will be processed by `theoplayer-backup-streams.js`.

![Index page](docs/index.png "Index page where you can upload your streams")
Furthermore,

* THEOplayer overlays the title when the video is paused.
* THEOplayer sets the poster image if no prior poster has been configured.
* THEOplayer leverages this information to enhance the offline-primary-stream-to-online-primary-stream-to-offline-primary-stream-to-online-backup-stream transitions. (See below.) 

## Enhanced offline-to-online-to-offline-to-online-backupstream transitions
The main use-case is to continuously stream live video.

The Lifecycle of Continuous Play using THEOplayer

    1. Add Several Live Streams (START)
    2. Start Each Live Stream
    3. Stop First Live Stream
    4. Verify Next Available Online Stream Starts Automatically
    5. Stop All Streams
    5. Delete All Live Streams (END)
  
  * When you add your live streams, your streams are offline.
  * Before you start your live streams, all your streams remain offline.
  * When you start all your live streams, your first live stream goes online.
  * When you stop your first live stream, your first live stream goes offline and your first online backup live stream remains online and takes over play.
  * When you stop your first backup live stream, a subsequent backup live stream (if specified) remains online and takes over play.
  * When you re-start your first live stream and stop all other live streams, your first live stream goes online and takes over play.
  * When you stop all your live streams, all your streams go offline.
</details>

When you supply a configuration, the following functionalities are unlocked:

* When all your live streams are offline, THEOplayer displays a placeholder image, which overlaps the video player.
* When your first live stream becomes available, THEOplayer automatically starts playing the stream.
* When your first live stream stops, THEOplayer automatically checks and plays the next available online backup stream.
* When all your backup streams are down but your first live stream is back up, THEOplayer automatically starts playing this stream.

What's shown inside of the placeholder?

* A single, configurable status message.

# Getting started

## 1. THEOplayer HTML5 SDK
Create a THEOplayer HTML5 SDK at https://portal.theoplayer.com/. Get familiar with the THEOplayer API at https://docs.portal.theoplayer.com/getting-started/01-sdks/01-web/00-getting-started.md.

## 2. Specify one or more live streams
To specify your backup stream(s), add a new JSON object, for each backup stream, in the `backupStreams` array of your HTML file.

## 3. Configure local demo
First of all, you must run the NPM scripts described below to install your NPM dependencies and to compile the 
previously mentioned `theoplayer-theoplayer-backup-streams.js`. Finally, you can run an NPM script to boot a local web server.

Once you have all of your library files, you want to swap in your THEOplayer library files in `public/index.html`.
(Head over to the [getting started guide](https://docs.portal.theoplayer.com/getting-started/01-sdks/01-web/00-getting-started.md) if you're unsure on how to achieve this.)

Then, you want to configure your live stream and configuration in `public/index.html`.

# Live Demo
A live demo is available at https://cdn.theoplayer.com/demos/backup-streams-demo/index.html.

# API of `theoplayer-backup-streams.js`

`THEOplayerSession`: the global variable which exposes a function to register a video player.
```javascript
var theoplayerSession = theoplayerSession.registerPlayer(player);
```
The `theoplayerSession` exposes two functions:
```javascript
theoplayerSession.getState(); // returns 'offline' or 'online'
theoplayerSession.getStreamData(); // returns the url response
```
You can use a `THEOplayerSessionConfiguration` when you have registered your player through `theoplayerSession.registerPlayer(player)`.
```javascript
    var element = document.querySelector('.theoplayer-container');
    var player = new THEOplayer.Player(element, {
        libraryLocation : '//cdn.theoplayer.com/dash/theoplayer/'
    });

    var theoplayerSession = theoplayerSession.registerPlayer(player);

    if ( document.getElementById('src_1').value.indexOf('.m3u8') !== -1 ) {
        type_1 = 'application/x-mpegurl';
    } else if ( document.getElementById('src_1').value.indexOf('.mpd') !== -1 ) {
        type_1 = 'application/dash+xml';
    } else {
        type_1 = '';
    }

    if ( document.getElementById('src_2').value.indexOf('.m3u8') !== -1 ) {
        type_2 = 'application/x-mpegurl';
    } else if ( document.getElementById('src_2').value.indexOf('.mpd') !== -1 ) {
        type_2 = 'application/dash+xml';
    } else {
        type_2 = '';
    }

    if ( document.getElementById('src_3').value.indexOf('.m3u8') !== -1 ) {
        type_3 = 'application/x-mpegurl';
    } else if ( document.getElementById('src_3').value.indexOf('.mpd') !== -1 ) {
        type_3 = 'application/dash+xml';
    } else {
        type_3 = '';
    }

    function getSourceDescription() {
        return {
            sources : [{
                src : document.getElementById('src_1').value,
                type : type_1
            }],
            metadata: {
                session: {
                    retry: document.getElementById('retry').value,
                    offlineText: document.getElementById('offlineText').value,
                    placeholderImageUrl: document.getElementById('placeholderImg').value,
                    statechangeCallback: console.log,
                    datachangeCallback: console.log,
                    backupStreams: [
                            { sources : [{
                                src : document.getElementById('src_2').value,
                                type : type_2
                                }],
                                metadata: {
                                    session: {
                                        retry: document.getElementById('retry').value,
                                        offlineText: document.getElementById('offlineText').value,
                                        placeholderImageUrl: document.getElementById('placeholderImg').value,
                                        statechangeCallback: console.log,
                                        datachangeCallback: console.log
                                    }
                                }
                            },
                            { sources : [{
                                src : document.getElementById('src_3').value,
                                type : type_3
                                }],
                                metadata: {
                                    session: {
                                        retry: document.getElementById('retry').value,
                                        offlineText: document.getElementById('offlineText').value,
                                        placeholderImageUrl: document.getElementById('placeholderImg').value,
                                        statechangeCallback: console.log,
                                        datachangeCallback: console.log
                                    }
                                }
                            },
                        ]
                    }
                }
        };
    }

    player.source = getSourceDescription();

    document.getElementById('update').addEventListener('click', function() {
        player.source = getSourceDescription();
        player.currentTime = Infinity; //seeking to Infinity
    })
```

In a `THEOplayerBackupSourceConfiguration`, you can configure the following flags:

- `retry`: the amount of milliseconds which the client should wait to check if the stream is back online.
- `offlineText`: the text which should be shown if the stream is unavailable, and if there's no `url`.
- `placeholderImageUrl`: the image which should be shown in the background when there is no `url`.
- `queryParametersPassthrough`: setting this value to true appends the query parameters of your HLS master playlist to all other requests (e.g. media playlists, segments, ...).


- `datachangeCallback`: the function which should be called when the response from `url` is available.
- `statechangeCallback`: the function which should be called when a stream either becomes available or unavailable.
- `backupStreams`: an array of your backup live streams, each containing their respective stream source and flag configurations as listed in the main `THEOplayerBackupSourceConfiguration`.

# NPM scripts
- Install NPM dependencies: `npm install`
- Build `js/theoplayer-backup-streams.js` with Webpack: `npm run build-dev` or `npn run build`
- Serve the app locally: `npm run start` (Open [http://localhost:8000](http://localhost:8000) to view it in the browser.)

# Future work
* Improve README.md.
* Refactor `/src`.
* Sometimes the HLS master playlist returns a 200 status code (from cache), but the media playlists are still 404s.
When this occurs, the client reloads the stream. While this is not wrong, it can trigger some transitions which could be
considered a visual glitch.